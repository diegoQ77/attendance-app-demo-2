import sys
import os
sys.path.insert(0, os.path.abspath('../'))

from src.logger_wrapper import setup_logger
from src.teams_files_reader import load_data # 1 import function to import all csv files
from prettytable import PrettyTable
from datetime import *

PINK = "\x1b[1;35m"
WHITE = "\033[37m"



logger =  setup_logger(__name__)


def main():
    star_time = datetime.now().strftime("%d %m %Y, %H %M %S %f")
    logger.debug("Start tools file converter program")
    get_csv_files = load_data()  # 1 call function to import all csv files
    logger.debug("End tools file converter program")
    end_time = datetime.now().strftime("%d %m %Y, %H %M %S %f")
    total_files = get_csv_files
    output_Pretty_summary(star_time, end_time, total_files)

def output_Pretty_summary(start, end, total_files):
    output_time = PrettyTable()
    output_time.field_names = ["Execution Start Time", "Execution End Time", "Total Files in Folder"]
    output_time.add_rows(
    [
        [start, end, total_files]   
    ]
    )
    print("\nSUMMARY: ")
    print(PINK,output_time)
    print(WHITE,".")


if __name__ == '__main__':
    main()































# import sys
# import os
# # read current file's directory; then make sure path is one level up
# print(os.path.dirname(__file__))
# print()
# path = os.path.dirname(__file__)[::-1].split('/', 1)[-1][::-1]

# # now put `path` in list sys.path
# sys.path.extend([path, f'{path}/src'])
# print(sys.path)

# import function setup_logger from module `logger_wrapper`.
# `logger_wrapper` is located in src folder


# from src.logger_wrapper import setup_logger
# from src.teams_files_reader import load_data

# logger = setup_logger(__name__)

# def main():
#     """orchestrates tools functionality"""
#     logger.info("starting main")
#     load_data()
#     logger.info("finishing main")

# if __name__ == '__main__':
#     main()
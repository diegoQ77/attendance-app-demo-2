from src.helpers import subtract_time_strType
from src.duration_helper import min_join_time, max_leave_time, calculate_time, sum_duration, format_duration, format_standar_time_date

EMAIL = 'Email'

def normalize_participant(raw_data):
    name = raw_data.get('Name') or raw_data.get('Full Name')
    star_time = raw_data.get('Join Time') or raw_data.get('Join time') or raw_data.get('First join')
    end_time = raw_data.get('Leave Time') or raw_data.get('Leave time') or raw_data.get('Last leave')
    duration = subtract_time_strType(star_time, end_time)
    email = raw_data.get('Email')
    id = raw_data.get('Email') if raw_data.get('Participant ID (UPN)') is None else raw_data.get('Participant ID (UPN)')
    role = raw_data.get('Role')
    return {
        'Name' : name,
        'First join' : star_time,
        'Last leave' : end_time,
        'In-meeting duration' : duration,
        'Email' : email,
        'Participant ID (UPN)' : id,
        'Role' : role
    } 

def resolve_participant_join_last_time(raw_data):

    list_join_time = [p[i] for p in raw_data for i in p if 'join' in i.lower()]
    list_leave_time = [p[i] for p in raw_data for i in p if 'leave' in i.lower()]

    join_time = str(min_join_time(list_join_time))
    leave_time = str(max_leave_time(list_leave_time))
    duration = ''
    for participant in raw_data:
        lapse = calculate_time(participant.get('Join Time'),participant.get('Leave Time'))
        duration = sum_duration(lapse,duration)
        
    f_duration = format_duration(duration)

    id = raw_data[0].get('Email') if raw_data[0].get('Participant ID (UPN)') is None else raw_data[0].get('Participant ID (UPN)')

    return {
        'Name' : raw_data[0].get('Full Name'),
        'First join' : format_standar_time_date(join_time),
        'Last leave' : format_standar_time_date(leave_time),
        'In-meeting duration' : f_duration,
        'Email' : raw_data[0].get('Email'),
        'Participant ID (UPN)' : id,
        'Role' : raw_data[0].get('Role')
    }

def build_participant_object(raw: dict):
    pass


def find_ocurrences(list_participants):
    return dict((i, list_participants.count(i)) for i in list_participants)


def get_list_all_participants(list_participants, key = EMAIL):
    return list(filter(None,[participant.get(key) for participant in list_participants]))

def participants_repeat(list_participants, key = EMAIL):
    list_all_participants = get_list_all_participants(list_participants)
    ocurrences = find_ocurrences(list_all_participants)
    list_with_repetitions= [v for v in ocurrences if ocurrences[v] > 1]
    return [participant for participant in list_participants if participant.get(key) in list_with_repetitions]

def participants_email_none(list_participants):
    return [participant for participant in list_participants if participant.get('Email') is None or participant.get('Email') == '']

def participants_no_repeat(list_participants, key = EMAIL):
    list_all_participants = get_list_all_participants(list_participants)
    ocurrences = find_ocurrences(list_all_participants)
    list_without_repetitions= [v for v in ocurrences if ocurrences[v] == 1]
    return [participant for participant in list_participants if participant.get(key) in list_without_repetitions]

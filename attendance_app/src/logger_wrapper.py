import logging


def setup_logger(logger_name, logfile="../logs/log.log"):
    # get logger
    logger = logging.getLogger(logger_name)
    
    # create file handler which logs even debug messages
    file_handler = logging.FileHandler(logfile)
    # file_handler.setLevel(logging.INFO)
    
    # # create console handler with a higher log level
    stream_handler = logging.StreamHandler()
    # stream_handler.setLevel(logging.WARNING)
    
    # # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    
    # set formater
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)
    
    # # add the handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    
    # set loggin level tothe logger
    logger.setLevel(logging.DEBUG)
    
    return logger
from datetime import *
from src.helpers import get_strformat, strip_time

def convert_duration_str_to_dict(duration_str):
    return dict()


def convert_dict_to_object():
    return Dictionary()

def erase_zeros (string_time):
    only_date = string_time.split(', ')[0]
    only_time = string_time.split(', ')[1]
    if only_date[0] == '0':
        only_date = only_date.lstrip(' 0')
    if only_time[0] == '0':
        only_time = only_time.lstrip(' 0')
    return f"{only_date}, {only_time}"
    
def format_standar_time_date(time):
    time_i = datetime.strptime(time,'%Y-%m-%d %H:%M:%S')
    return erase_zeros(time_i.strftime('%m/%d/%y, %I:%M:%S %p'))

def standar_datetime(time):
    year = time.split(', ')[0].split('/')
    if len(year[-1]) == 4:
        return datetime.strptime(time,"%m/%d/%Y, %I:%M:%S %p")
    if len(year[-1]) == 2:
        return datetime.strptime(time,"%m/%d/%y, %I:%M:%S %p")


def sum_duration (time, duration):
    if duration == '': return time
    return time+duration

def format_duration(duration):
    hours, minutes, seconds = strip_time(duration)
    output = ''
    if hours != 0: output += f'{hours}h '
    if minutes != 0: output += f'{minutes}m '
    if seconds != 0: output += f'{seconds}s'
    return output

def calculate_time(date_one, date_two):
    str_format =  get_strformat(date_one)
    time_start = datetime.strptime(date_one, str_format) 
    time_end = datetime.strptime(date_two, str_format) 
    return time_end - time_start


def min_join_time(list_join_time):
    #new_list = [standar_datetime(str(jtt)) for jtt in list_join_time]
    #get_strformat(date)
    new_list = list(map(standar_datetime,list_join_time))
    #new_list = list(map(get_strformat,list_join_time))
    join_time = new_list[0]
    for jt in new_list:
        join_time_aux = jt
        if join_time < join_time_aux: join_time = join_time
        else: join_time = join_time_aux
    return join_time


def max_leave_time(list_leave_time):
    #new_list = [standar_datetime(str(jtt)) for jtt in list_leave_time]
    new_list = list(map(standar_datetime,list_leave_time))
    leave_time = new_list[0]
    for jt in new_list:
        leave_time_aux = jt
        if leave_time > leave_time_aux: leave_time = leave_time
        else: leave_time = leave_time_aux
    return leave_time
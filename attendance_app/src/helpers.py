from os import listdir
from os.path import isfile, join
import csv
from datetime import *


def discover_data_files(path='../data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if isfile(join(path, f)) and f.endswith('csv')]


def read_file(files):
    readers = []
    for  f in files:
        csv_file = open(f, encoding='UTF-16')
        readers.append((f, csv.reader(csv_file, delimiter='\t')))
    return readers

def subtract_time(date_one, date_two):
    str_format =  get_strformat(date_one)
    time_start = datetime.strptime(date_one, str_format) 
    time_end = datetime.strptime(date_two, str_format) 
    time = time_end - time_start
    hours, minutes, seconds = strip_time(time)
    return {
        'Hours': hours, 
        'Minutes': minutes,
        'Seconds': seconds
    }

def subtract_time_strType(date_one, date_two):
    if date_one is None:
        return []
    str_format =  get_strformat(date_one)
    time_start = datetime.strptime(date_one, str_format) 
    time_end = datetime.strptime(date_two, str_format) 
    time = time_end - time_start
    hours, minutes, seconds = strip_time(time)
    str_duration = duration_string_format(hours, minutes, seconds)
    return str_duration 


def duration_string_format(hours, minutes,seconds):
    str_duration = ''
    if hours != 0: str_duration += f'{hours}h '
    if minutes != 0: str_duration += f'{minutes}m '
    if seconds != 0: str_duration += f'{seconds}s'
    return str_duration


def get_strformat(date):
    return '%m/%d/%Y, %I:%M:%S %p' if '2022' in date else '%m/%d/%y, %I:%M:%S %p'

def strip_time(time):
    seconds = time.seconds  # returns total amount of seconds
    hours, seconds = divmod(seconds, 3600) # hours gets the integer value of seconds/3600 and seconds get the residue of the operation
    minutes, seconds = divmod(seconds, 60) # minutes gets the integer value of  seconds/60 and seconds get the residue
    return hours, minutes, seconds



#print(subtract_time_strType("9/8/22, 8:30:14 AM", "9/8/22, 10:01:55 AM"))
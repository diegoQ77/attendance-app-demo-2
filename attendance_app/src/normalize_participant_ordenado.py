YELLOW = "\x1b[1;33m"
GREEN = '\x1b[1;32m'
BLUE = "\x1b[1;34m"
PINK = "\x1b[1;35m"
WHITE = "\033[1;37m"

EMAIL = 'Email'
from datetime import *

#lista si participante viene vacio
list_v1 = [{'Full Name': 'Salome Quispe Guarachi', 'Join Time': '10/18/2022, 5:50:03 PM', 'Leave Time': '10/18/2022, 7:53:52 PM', 'Duration': '2h 3m', 'Email': 'Salome.Quispe@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Salome.Quispe@fundacion-jala.org'}, 
{'Full Name': 'Emily Cadena Quisbert', 'Join Time': '10/18/2022, 5:51:11 PM', 'Leave Time': '10/18/2022, 7:53:49 PM', 'Duration': '2h 2m', 'Email': 'Emily.Cadena@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Emily.Cadena@fundacion-jala.org'}, 
{'Full Name': 'Maria Marquina Guevara', 'Join Time': '10/18/2022, 5:52:16 PM', 'Leave Time': '10/18/2022, 7:53:52 PM', 'Duration': '2h 1m', 'Email': 'Maria.Marquina@fundacion-jala.org', 'Role': 'Organizer', 'Participant ID (UPN)': 'Maria.Marquina@fundacion-jala.org'}, 
{'Full Name': 'Daniel Moscoso Sanzetenea', 'Join Time': '10/18/2022, 5:53:21 PM', 'Leave Time': '10/18/2022, 7:53:39 PM', 'Duration': '2h ', 'Email': 'Daniel.Moscoso@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Daniel.Moscoso@fundacion-jala.org'}, 
{'Full Name': 'Andre Amurrio Vargas', 'Join Time': '10/18/2022, 5:54:09 PM', 'Leave Time': '10/18/2022, 7:41:52 PM', 'Duration': '1h 47m', 'Email': 'Andre.Amurrio@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Andre.Amurrio@fundacion-jala.org'}, 
{'Full Name': 'Patricio Rodriguez Lazarte', 'Join Time': '10/18/2022, 5:54:53 PM', 'Leave Time': '10/18/2022, 7:53:34 PM', 'Duration': '1h 58m', 'Email': 'Patricio.Rodriguez@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Patricio.Rodriguez@fundacion-jala.org'}, 
{'Full Name': 'Katerin Apaza Romero', 'Join Time': '10/18/2022, 5:56:03 PM', 'Leave Time': '10/18/2022, 6:17:24 PM', 'Duration': '21m 21s', 'Email': 'Katerin.Apaza@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Katerin.Apaza@fundacion-jala.org'}, 
{'Full Name': 'Katerin Apaza Romero', 'Join Time': '10/18/2022, 6:47:36 PM', 'Leave Time': '10/18/2022, 7:39:11 PM', 'Duration': '51m 34s', 'Email': 'Katerin.Apaza@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Katerin.Apaza@fundacion-jala.org'}, 
{'Full Name': 'Santiago Martinez Saavedra', 'Join Time': '10/18/2022, 5:58:32 PM', 'Leave Time': '10/18/2022, 6:04:45 PM', 'Duration': '6m 13s', 'Email': 'Santiago.Martinez@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Santiago.Martinez@fundacion-jala.org'}, 
{'Full Name': 'Santiago Martinez Saavedra', 'Join Time': '10/18/2022, 7:26:00 PM', 'Leave Time': '10/18/2022, 7:53:36 PM', 'Duration': '27m 35s', 'Email': 'Santiago.Martinez@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Santiago.Martinez@fundacion-jala.org'}, 
{'Full Name': 'Gabriel Cabero Torrico', 'Join Time': '10/18/2022, 5:58:38 PM', 'Leave Time': '10/18/2022, 7:53:38 PM', 'Duration': '1h 54m', 'Email': 'Gabriel.Cabero@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Gabriel.Cabero@fundacion-jala.org'}, 
{'Full Name': 'Diego Terrazas Sanchez', 'Join Time': '10/18/2022, 6:01:23 PM', 'Leave Time': '10/18/2022, 7:53:37 PM', 'Duration': '1h 52m', 'Email': 'Diego.Terrazas@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Diego.Terrazas@fundacion-jala.org'}, 
{'Full Name': 'Adriano Quiroga Encinas', 'Join Time': '10/18/2022, 6:02:16 PM', 'Leave Time': '10/18/2022, 7:53:36 PM', 'Duration': '1h 51m', 'Email': 'Adriano.Quiroga@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Adriano.Quiroga@fundacion-jala.org'}, 
{'Full Name': 'Walker Peredo Escobar', 'Join Time': '10/18/2022, 6:03:30 PM', 'Leave Time': '10/18/2022, 7:43:53 PM', 'Duration': '1h 40m', 'Email': 'Walker.Peredo@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Walker.Peredo@fundacion-jala.org'}, 
{'Full Name': 'Walker Peredo Escobar', 'Join Time': '10/18/2022, 7:46:03 PM', 'Leave Time': '10/18/2022, 7:53:33 PM', 'Duration': '7m 30s', 'Email': 'Walker.Peredo@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Walker.Peredo@fundacion-jala.org'}, 
{'Full Name': 'Daniel Saucedo Gutierrez', 'Join Time': '10/18/2022, 6:03:50 PM', 'Leave Time': '10/18/2022, 6:06:20 PM', 'Duration': '2m 30s', 'Email': 'Daniel.Saucedo@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Daniel.Saucedo@fundacion-jala.org'}, 
{'Full Name': 'Fernando Sivila Ramirez', 'Join Time': '10/18/2022, 6:05:34 PM', 'Leave Time': '10/18/2022, 7:29:48 PM', 'Duration': '1h 24m', 'Email': 'Fernando.Sivila@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Fernando.Sivila@fundacion-jala.org'}, 
{'Full Name': 'Isaac Enriquez Ona', 'Join Time': '10/18/2022, 6:10:02 PM', 'Leave Time': '10/18/2022, 7:53:40 PM', 'Duration': '1h 43m', 'Email': 'Isaac.Enriquez@fundacion-jala.org', 'Role': 'Presenter', 'Participant ID (UPN)': 'Isaac.Enriquez@fundacion-jala.org'}, 
{'Full Name': 'Santiago Martinez Saavedra', 'Join Time': '10/18/2022, 6:13:12 PM', 'Leave Time': '10/18/2022, 6:13:38 PM', 'Duration': '25s', 'Email': '', 'Role': 'Presenter', 'Participant ID (UPN)': ''}, 
{'Full Name': 'Santiago Martinez Saavedra', 'Join Time': '10/18/2022, 6:16:31 PM', 'Leave Time': '10/18/2022, 7:26:05 PM', 'Duration': '1h 9m', 'Email': '', 'Role': 'Presenter', 'Participant ID (UPN)': ''}]

print(BLUE+f"{len(list_v1)}",WHITE)

#lista con participant
list_v2 = [{'Name': 'Salome Quispe Guarachi', 'Join time': '10/18/22, 5:50:03 PM', 'Leave time': '10/18/22, 7:53:52 PM', 'Duration': '2h 3m 48s', 'Email': 'Salome.Quispe@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Emily Cadena Quisbert', 'Join time': '10/18/22, 5:51:11 PM', 'Leave time': '10/18/22, 7:53:49 PM', 'Duration': '2h 2m 37s', 'Email': 'Emily.Cadena@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Maria Marquina Guevara', 'Join time': '10/18/22, 5:52:16 PM', 'Leave time': '10/18/22, 7:53:52 PM', 'Duration': '2h 1m 35s', 'Email': 'Maria.Marquina@fundacion-jala.org', 'Role': 'Organizer'}, 
{'Name': 'Daniel Moscoso Sanzetenea', 'Join time': '10/18/22, 5:53:21 PM', 'Leave time': '10/18/22, 7:53:39 PM', 'Duration': '2h 17s', 'Email': 'Daniel.Moscoso@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Andre Amurrio Vargas', 'Join time': '10/18/22, 5:54:09 PM', 'Leave time': '10/18/22, 7:41:52 PM', 'Duration': '1h 47m 43s', 'Email': 'Andre.Amurrio@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Patricio Rodriguez Lazarte', 'Join time': '10/18/22, 5:54:53 PM', 'Leave time': '10/18/22, 7:53:34 PM', 'Duration': '1h 58m 40s', 'Email': 'Patricio.Rodriguez@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Katerin Apaza Romero', 'Join time': '10/18/22, 5:56:03 PM', 'Leave time': '10/18/22, 6:17:24 PM', 'Duration': '21m 21s', 'Email': 'Katerin.Apaza@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Katerin Apaza Romero', 'Join time': '10/18/22, 6:47:36 PM', 'Leave time': '10/18/22, 7:39:11 PM', 'Duration': '51m 34s', 'Email': 'Katerin.Apaza@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Santiago Martinez Saavedra', 'Join time': '10/18/22, 5:58:32 PM', 'Leave time': '10/18/22, 6:04:45 PM', 'Duration': '6m 13s', 'Email': 'Santiago.Martinez@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Santiago Martinez Saavedra', 'Join time': '10/18/22, 7:26:00 PM', 'Leave time': '10/18/22, 7:53:36 PM', 'Duration': '27m 35s', 'Email': 'Santiago.Martinez@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Santiago Martinez Saavedra', 'Join time': '10/18/22, 6:13:12 PM', 'Leave time': '10/18/22, 6:13:38 PM', 'Duration': '25s', 'Email': '', 'Role': 'Presenter'}, 
{'Name': 'Santiago Martinez Saavedra', 'Join time': '10/18/22, 6:16:31 PM', 'Leave time': '10/18/22, 7:26:05 PM', 'Duration': '1h 9m 33s', 'Email': '', 'Role': 'Presenter'}, 
{'Name': 'Gabriel Cabero Torrico', 'Join time': '10/18/22, 5:58:38 PM', 'Leave time': '10/18/22, 7:53:38 PM', 'Duration': '1h 54m 59s', 'Email': 'Gabriel.Cabero@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Diego Terrazas Sanchez', 'Join time': '10/18/22, 6:01:23 PM', 'Leave time': '10/18/22, 7:53:37 PM', 'Duration': '1h 52m 13s', 'Email': 'Diego.Terrazas@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Adriano Quiroga Encinas', 'Join time': '10/18/22, 6:02:16 PM', 'Leave time': '10/18/22, 7:53:36 PM', 'Duration': '1h 51m 20s', 'Email': 'Adriano.Quiroga@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Walker Peredo Escobar', 'Join time': '10/18/22, 6:03:30 PM', 'Leave time': '10/18/22, 7:43:53 PM', 'Duration': '1h 40m 22s', 'Email': 'Walker.Peredo@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Walker Peredo Escobar', 'Join time': '10/18/22, 7:46:03 PM', 'Leave time': '10/18/22, 7:53:33 PM', 'Duration': '7m 30s', 'Email': 'Walker.Peredo@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Daniel Saucedo Gutierrez', 'Join time': '10/18/22, 6:03:50 PM', 'Leave time': '10/18/22, 6:06:20 PM', 'Duration': '2m 30s', 'Email': 'Daniel.Saucedo@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Fernando Sivila Ramirez', 'Join time': '10/18/22, 6:05:34 PM', 'Leave time': '10/18/22, 7:29:48 PM', 'Duration': '1h 24m 13s', 'Email': 'Fernando.Sivila@fundacion-jala.org', 'Role': 'Presenter'}, 
{'Name': 'Isaac Enriquez Ona', 'Join time': '10/18/22, 6:10:02 PM', 'Leave time': '10/18/22, 7:53:40 PM', 'Duration': '1h 43m 38s', 'Email': 'Isaac.Enriquez@fundacion-jala.org', 'Role': 'Presenter'}]
print(BLUE,len(list_v2),WHITE)

##############################################################################################
###############################################################################################



###################################################################################33
#_______________________FILE helpers.py_____________________________________


def get_strformat(date):
    return '%m/%d/%Y, %I:%M:%S %p' if '2022' in date else '%m/%d/%y, %I:%M:%S %p'

def strip_time(time):
    seconds = time.seconds  # returns total amount of seconds
    hours, seconds = divmod(seconds, 3600) # hours gets the integer value of seconds/3600 and seconds get the residue of the operation
    minutes, seconds = divmod(seconds, 60) # minutes gets the integer value of  seconds/60 and seconds get the residue
    return hours, minutes, seconds


##################################################################################################
#_______________________FILE teams_file_reader.py_____________________________________

#lista de todos los email participantes sin vacios
def get_list_all_participants(list_participants, key = EMAIL):
    return list(filter(None,[participant.get(key) for participant in list_participants]))


def get_no_repeted_list(list_participants, key = EMAIL):
        return list(set([participant.get(key) for participant in list_participants]))

def find_ocurrences(list_participants):
    return dict((i, list_participants.count(i)) for i in list_participants)


#lista participantes repetidos
def participants_repeat(list_participants, key = EMAIL):
    list_all_participants = get_list_all_participants(list_participants)
    ocurrences = find_ocurrences(list_all_participants)
    list_with_repetitions= [v for v in ocurrences if ocurrences[v] > 1]
    return [participant for participant in list_participants if participant.get(key) in list_with_repetitions]

#  lista participantes no repetidos
def participants_no_repeat(list_participants, key = EMAIL):
    list_all_participants = get_list_all_participants(list_participants)
    ocurrences = find_ocurrences(list_all_participants)
    list_without_repetitions= [v for v in ocurrences if ocurrences[v] == 1]
    return [participant for participant in list_participants if participant.get(key) in list_without_repetitions]


#participantes que tienen un none en su email
def participants_email_none(list_participants):
    return [participant for participant in list_participants if participant.get('Email') is None or participant.get('Email') == '']

###################################################################################33
#_______________________RESOLVE JOIN TIME Y LEAVE TIME VERSION OLD_____________________________________
#__________________________FILE participant_builer.py_____________________________________

def standar_datetime(time):
    year = time.split(', ')[0].split('/')
    if len(year[-1]) == 4:
        return datetime.strptime(time,"%m/%d/%Y, %I:%M:%S %p")
    if len(year[-1]) == 2:
        return datetime.strptime(time,"%m/%d/%y, %I:%M:%S %p")

def min_join_time(list_join_time):
    #new_list = [standar_datetime(str(jtt)) for jtt in list_join_time]
    new_list = list(map(standar_datetime,list_join_time))
    join_time = new_list[0]
    for jt in new_list:
        join_time_aux = jt
        if join_time < join_time_aux:
            join_time = join_time
        else:
            join_time = join_time_aux
    return join_time

def max_leave_time(list_leave_time):
    #new_list = [standar_datetime(str(jtt)) for jtt in list_leave_time]
    new_list = list(map(standar_datetime,list_leave_time))
    leave_time = new_list[0]
    for jt in new_list:
        leave_time_aux = jt
        if leave_time > leave_time_aux:
            leave_time = leave_time
        else:
            leave_time = leave_time_aux
    return leave_time

def subtract_time_2(date_one, date_two):
    str_format =  get_strformat(date_one)
    time_start = datetime.strptime(date_one, str_format) 
    time_end = datetime.strptime(date_two, str_format) 
    return time_end - time_start
    
def sum_duration (time, duration):
    if duration == '': return time
    return time+duration

def format_duration(duration):
    hours, minutes, seconds = strip_time(duration)
    output = ''
    if hours != 0: output += f'{hours}h '
    if minutes != 0: output += f'{minutes}m '
    if seconds != 0: output += f'{seconds}s'
    return output

def resolve_participant_join_last_time(raw_data):

    list_join_time = [p[i] for p in raw_data for i in p if 'join' in i.lower()]
    list_leave_time = [p[i] for p in raw_data for i in p if 'leave' in i.lower()]

    #max y min sin formato de salida
    join_time = str(min_join_time(list_join_time))
    leave_time = str(max_leave_time(list_leave_time))
    duration = ''
    for participant in raw_data:
        lapse = subtract_time_2(participant.get('Join Time'),participant.get('Leave Time'))
        duration = sum_duration(lapse,duration)
    f_duration = format_duration(duration)

    id = raw_data[0].get('Email') if raw_data[0].get('Participant ID (UPN)') is None else raw_data[0].get('Participant ID (UPN)')

    return {
        'Name' : raw_data[0].get('Full Name'),
        'First join' : join_time,
        'Last leave' : leave_time,
        'In-meeting duration' : f_duration,
        'Email' : raw_data[0].get('Email'),
        'Participant ID (UPN)' : id,
        'Role' : raw_data[0].get('Role')
    }
################################################################################
#__________________________FILE participant_builer.py_____________________________________
# ############################################################################33
def normalize_participant(raw_data):
    name = raw_data.get('Name') or raw_data.get('Full Name')
    star_time = raw_data.get('Join Time') or raw_data.get('Join time')
    end_time = raw_data.get('Leave Time') or raw_data.get('Leave time')
    duration = subtract_time_2(star_time, end_time)
    f_duration = format_duration(duration)
    email = raw_data.get('Email')
    id = raw_data.get('Email') if raw_data.get('Participant ID (UPN)') is None else raw_data.get('Participant ID (UPN)')
    role = raw_data.get('Role')
    return {
        'Name' : name,
        'First join' : star_time,
        'Last leave' : end_time,
        'In-meeting duration' : f_duration,
        'Email' : email,
        'Participant ID (UPN)' : id,
        'Role' : role
    }    
        
##########################################################################################
#_____________________________NORMALIZE OLD VERSION_____________________________________
#_______________________FILE teams_file_reader.py_____________________________________
def normalize_old_version(list_participants,key = EMAIL):

    list_participants_email_none = participants_email_none(list_participants)
    list_participants_no_repeat = participants_no_repeat(list_participants)
    list_participants_repeat_raw_data = participants_repeat(list_participants)

    list_participants_repeat_normalized = []
    list_participants_email_none_normalized = []
    list_participants_no_repeat_normalized = []
    big_list_participants = []

    lnr = list(set(get_list_all_participants(list_participants_repeat_raw_data)))

    print(lnr)

    for participant in list_participants_no_repeat:
        big_list_participants.append(normalize_participant(participant))

    for _,participant in enumerate(lnr):
        #print(i)
        lista = [p_r for p_r in list_participants_repeat_raw_data  if participant == p_r.get(key)]
        #print(lista)
        big_list_participants.append(resolve_participant_join_last_time(lista))

    
    for participant in list_participants_email_none:
        big_list_participants.append(normalize_participant(participant))


    print(GREEN,big_list_participants,WHITE)
    return big_list_participants


###################################################################################33
#_____________________________NORMALIZE NEW VERSION_____________________________________
#_______________________FILE teams_file_reader.py_____________________________________
def normalize_new_version(list_participants):
    list_participants_normalized_new_version = []
    for participant in list_participants:
        list_participants_normalized_new_version.append(participant)
    return list_participants_normalized_new_version

###################################################################################33
#_____________________________FUNCION PRINCIPAL_____________________________________
#_______________________FILE teams_file_reader.py_____________________________________
#funcion principal
def normalize_participants(list_participants, list_in_meeting_participant):
    list_participants_normalized = []
    if list_participants == []:
        list_participants_normalized = normalize_old_version(list_in_meeting_participant)
    else:
        list_participants_normalized = normalize_new_version(list_participants)
    return list_participants_normalized

normalize_participants([],list_v1)
from datetime import datetime
import os 
import csv
from src.logger_wrapper import setup_logger

logger = setup_logger(__name__)
# CONTANTS
STAGED_DATA_FOLDER = "staged_data_folder"

# get target name folder
def get_current_date():
  return datetime.now().strftime("%Y%m%d%H%M%S")


def create_staged_folder(directory = "../staged_data_folder"):
  if not os.path.exists(directory):
    os.makedirs('../staged_data_folder')


def create_folder_with_current_date(staged_path):
  new_folder_output = f'../{staged_path}/{get_current_date()}'
  if not os.path.exists(new_folder_output):
    os.makedirs(new_folder_output)
  return f'{staged_path}/{get_current_date()}'
    
    
def create_output_csv_file(data, file_path_in_proccess, staged_current_date_path):
  name_csv_file_output_path = file_path_in_proccess.split("\\")[-1] if  "\\" in file_path_in_proccess else file_path_in_proccess.split("/")[-1]
  write_in_new_csv_file(staged_current_date_path, name_csv_file_output_path, data)
      
      
def write_in_new_csv_file(staged_current_date_path, file_csv_output_path, data = {}):
    if data is {}:
      return
    try:
      logger.info(f"Write normalize in output csv, csv file name: {file_csv_output_path}")
      # put conditional  if os path exists mkdirs 
      with open(f'../{staged_current_date_path}/{file_csv_output_path}', 'w', newline='') as file:
        writer = csv.writer(file, delimiter="\t")
        # Write summary
        summary_dict = data.get("summary")
        write_summary_in_csv(writer,summary_dict)
        # Write participants
        participants_dict = data.get("participants")
        write_participants_in_csv_output(file, participants_dict, writer, "2. Participants")
        # Write in meeting activities
        in_meeting_list = data.get("in_meeting_activities")
        write_participants_in_csv_output(file, in_meeting_list, writer, "3. In-Meeting activities")

    except Exception as e:
      logger.exception(f"Error ocurrent trying to write in file: {file_csv_output_path}, erroy type: {e}")
  
    
def build_succes_folder(total_participants, staged_current_date_path, file_csv_output_path, has_data = True):
  logger.info(f"Add data in succes folder, file name: {file_csv_output_path}")
  
  target_path = f"../{staged_current_date_path}/succes.txt" 
  mode = "a" if has_data else "w"
  line = f"{total_participants},{file_csv_output_path} \n" if has_data else "0"
  
  with open(target_path, mode) as f:
    f.write(line)
  
      
def build_error_folder(staged_current_date_path, file_csv_output_path, has_error = False):
  logger.info(f"Add errors for folder: {file_csv_output_path}")

  target_path = f"../{staged_current_date_path}/error.txt" 
  mode = "a" if has_error else "w"
  line = f"error in file - {file_csv_output_path} \n" if has_error else "0"
  
  with open(target_path, mode) as f:
    f.write(line)
    

def write_summary_in_csv(writer, summary_dict):
  logger.info("Write in new csv summary section")
  
  writer.writerow(["1. Summary"])
  for key, value in summary_dict.items():
    writer.writerow([key, value])
  writer.writerow([])


def write_participants_in_csv_output(file, participants_dict, writer_line, header):
  logger.info("Write in new csv participants section")
  
  writer_line.writerow([header])
  headeres_participants = list(participants_dict[0].keys())
  writer = csv.DictWriter(file, fieldnames=headeres_participants, delimiter='\t') 
  writer.writeheader()
  writer.writerows(participants_dict)
  writer_line.writerow([])
  


      
from .helpers import subtract_time_strType
from src.logger_wrapper import setup_logger

logger = setup_logger(__name__)


def normalize_summary(raw_data: dict):
    logger.info(f"Start normalizing summary")
    try:
        MEETING_TITTLE = "Meeting title"
        start_time = raw_data.get('Meeting Start Time') or raw_data.get('Start time')
        end_time = raw_data.get('Meeting End Time') or raw_data.get('End time')
        duration = subtract_time_strType(start_time, end_time)  
        participants = raw_data.get('Total Number of Participants') or raw_data.get('Attended participants')
        title =  raw_data.get('Meeting Title') or raw_data.get(MEETING_TITTLE)
        id = raw_data.get('Meeting Id') or raw_data.get(MEETING_TITTLE)
        return {
                'Title': title,
                'Id': id,
                'Attended participants': participants,
                'Start Time': start_time,
                'End Time': end_time,
                'Duration': duration
        }
    
    except Exception as e:
        logger.exception(f"Error when trying to normalize data, error type: {e}")
        


def build_summary_object(raw_data: dict):
    pass